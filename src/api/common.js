import { $me } from '@/core/mixins/useMsg'
import { $get } from '@/core/mixins/useRequest'



export async function getDictByCode(code) {
    let { success, data } = await $get("/dict/get-val-by-code", {
        code,
    });
    if (!success) return $me("请求失败");
    return data
}

export async function getDictByModule(code) {
    let { success, data } = await $get("/dict/get-module", {
        code,
    });
    if (!success) return $me("请求失败");
    return data
}