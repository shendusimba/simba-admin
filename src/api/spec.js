import { $get } from '@/core/mixins/useRequest'

export async function getSpecValues(specId) {
    let { success, data } = await $get("/goodsSpecValue/list", {
        specId
    });
    if (!success) return $me("上传失败");
    return data
}