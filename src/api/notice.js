import { $get } from '~mixins/useRequest'

export function getUnReadCount() {
    return $get('/notice/unread-count')
}

export function getUnReadList({ pageIndex, pageSize, receiverType }) {
    return $get('/notice/unread-list', { pageIndex, pageSize, receiverType })
}
