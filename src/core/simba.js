
if (process.env.NODE_ENV === 'production')
    console.log(`%c
███████╗██╗███╗   ███╗██████╗  █████╗        █████╗ ██████╗ ███╗   ███╗██╗███╗   ██╗
██╔════╝██║████╗ ████║██╔══██╗██╔══██╗      ██╔══██╗██╔══██╗████╗ ████║██║████╗  ██║
███████╗██║██╔████╔██║██████╔╝███████║█████╗███████║██║  ██║██╔████╔██║██║██╔██╗ ██║
╚════██║██║██║╚██╔╝██║██╔══██╗██╔══██║╚════╝██╔══██║██║  ██║██║╚██╔╝██║██║██║╚██╗██║
███████║██║██║ ╚═╝ ██║██████╔╝██║  ██║      ██║  ██║██████╔╝██║ ╚═╝ ██║██║██║ ╚████║
╚══════╝╚═╝╚═╝     ╚═╝╚═════╝ ╚═╝  ╚═╝      ╚═╝  ╚═╝╚═════╝ ╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝@2020-2021`, 'color:#006699');

/**
 * 计算简单数组的笛卡尔积
 * [[1, 2, 3],[11, 22, 33],[111, 222, 333]]
 * @param {*} data 
 * @returns 
 */
export function parseDescartesFromSimpleArr(data) {
    return data.reduce((total, r) => r.reduce((row, rr) => {
        total.forEach(t => Array.isArray(t) && row.push([...t, rr]) || row.push([t, rr]))
        return row
    }, []))
}

/**
 * 计算对象数组的笛卡尔积
 *      [
            { label: "颜色", values: ["红色", "蓝色"] },
            { label: "尺寸", values: ["AAA", "BBB"] },
            { label: "内存", values: ["ccc", "ddd"] },
        ];
 */
export function parseDescartesFromObjArr(data, filedKey, dataKey) {
    return parseDescartesFromSimpleArr(data.map(r => r[dataKey].map(rr => ({ [r[filedKey]]: rr })))).map(r => Array.isArray(r) ? r.reduce((t, r) => Object.assign(t, r), {}) : r)
}

export function isFunction(obj) {
    return typeof obj === 'function'
}

export function distictBy(key) {
    let res = a.reduce((total, r) => {
        if (!total[r[key]]) total[r[key]] = r
        return total
    }, {})
    return Object.values(res)
}

export let notUndef = obj => obj !== undefined

/**
* 图像转blob
*/
export function getBlobImage(url, callback) {
    var xhr = new XMLHttpRequest()
    xhr.open('get', url, true)
    xhr.responseType = 'blob'
    xhr.onload = () => callback(xhr.response)
    xhr.send()
}

/**
* 图像转Base64
*/
export function getBase64Image(src, callback) {
    let img = new Image()
    img.setAttribute("crossOrigin", 'anonymous')
    img.src = src
    img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, img.width, img.height);
        var ext = img.src.substring(img.src.lastIndexOf(".") + 1).toLowerCase();
        callback(canvas.toDataURL("image/" + ext));
    }
}

/**
 * 图像blob转Base64
 */
export function convertBlob2Base64(blob, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(blob);
}