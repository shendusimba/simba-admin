import * as Icons from '@ant-design/icons-vue'
import { kebabCase } from 'lodash'
//图标注册中心  

export default (app) => {
    Object.keys(Icons).forEach(key => {
        let iconName = 'i-' + kebabCase(key.replace('Outlined', ''))
        app.component(iconName, Icons[key])
    })
}
