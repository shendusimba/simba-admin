import axios from 'axios'
import NProgress from 'nprogress'
import { getToken } from '@/utils/userToken';
import { notification } from 'ant-design-vue'
import COS from "@/utils/cos-js-sdk-v5";

import _ from 'love-u'

import { AJAX_BASE_URL, AJAX_TIMEOUT, BASE_URL_LIST, UPLOAD_URL } from '@/config'

var instance = axios.create({
    baseURL: AJAX_BASE_URL,
    timeout: AJAX_TIMEOUT
});

// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    NProgress.start()
    let { method, url, params, data } = config

    let token = getToken()
    if (token) config.headers.common['token'] = token;

    //绝对路径则不处理
    if (!url.startsWith('http')) {
        let urlSplitRes = url.split('/')
        let baseUrlType = urlSplitRes.shift()
        if (baseUrlType) {
            //通过前缀来获取对应的后台API-BASEURL
            config.baseURL = BASE_URL_LIST[baseUrlType]
            config.url = urlSplitRes.join('/')
        }
        //说明是访问网站内的public文件夹
        if (url.startsWith('dev'))
            config.url = location.origin + url.replace('dev', '')
    }
    // 在发送请求之前打印日志
    params = params || data
    try {
        let str = params && Object.keys(params).length ? decodeURIComponent(_.convertObj2Str(params)) : ""
        console.fakeLog(`${method}请求了${url}${str ? ',参数为' + str : ''}`);
    } catch (error) {
        console.log('参数qs转换出错，但不要紧', error);
    }
    return config;
}, function ({ message }) {
    NProgress.done()
    notification.error({
        message
    })
    return Promise.reject(message);
});

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    NProgress.done()
    console.fakeLog('响应为', response.data);
    return response;
}, function ({ message }) {
    NProgress.done()
    notification.error({
        message
    })
    return Promise.reject(message);
});

export const $get = async (url, params) => {
    let { data } = await instance.get(url, { params })
    return data
}

export const $post = async (url, params) => {
    let { data } = await instance.post(url, params)
    return data
}

export const $upload = async (file, fileName) => {
    if (file.size > 1024 * 1024 * 2) return $reject("文件不能大于2MB");
    let fd = new FormData()
    fd.append('file', file, fileName)
    return $post(UPLOAD_URL, fd)
    //return new Promise((resolve, reject) => {
    // cos.uploadFiles({
    //     files: [
    //         {
    //             Bucket: TXY_COS_Bucket,
    //             Region: TXY_COS_Region,
    //             Key: "" + +new Date(),
    //             Body: file,
    //         },
    //     ],
    //     SliceSize: 1024 * 1024 * 10 /* 设置大于10MB采用分块上传 */,
    //     onProgress,
    //     onFileFinish: function (err, data, options) {
    //         if (err) return reject("上传失败");
    //         cos.getObjectUrl(
    //             {
    //                 Bucket: TXY_COS_Bucket,
    //                 Region: TXY_COS_Region,
    //                 Key: options.Key,
    //                 Sign: false,
    //                 Expires: 0,
    //             },
    //             (err, { Url }) => {
    //                 if (err) return reject("失败");
    //                 resolve(Url)
    //             }
    //         );
    //     },
    // });
    //})
}

export const $put = async (url, params) => {
    let { data } = await instance.put(url, params)
    return data
}

export const $del = async (url, params) => {
    let { data } = await instance.delete(url, params)
    return data
}