import * as msg from './useMsg'
import * as request from './useRequest'
import * as utils from './useUtils'
import * as path from './usePath'
import * as filters from './useFilter'

export default (app) => app.mixin({ methods: { ...msg, ...request, ...utils, ...filters, ...path, } })