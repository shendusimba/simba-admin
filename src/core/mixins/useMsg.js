import {
    notification,
    message,
    Modal
} from 'ant-design-vue';
import {
    createVNode
} from 'vue'
import {
    ExclamationCircleOutlined
} from '@ant-design/icons-vue'

export function $msg(type, message, description) {
    return new Promise((resolve) => {
        notification[type]({
            message,
            onClose: () => resolve(),
            description,
            duration: 2,
        });
    })
}

export function $ms(message = '操作成功', description = '') {
    return $msg('success', message, description)
}

export function $mi(message = '提示', description = '') {
    return $msg('info', message, description)
}

export function $mw(message, description) {
    return $msg('warning', message, description)
}

export function $me(description = '', message = '操作失败') {
    return $msg('error', message, description)
}

export function $ml(msg, delay = 1) {
    return message.loading(msg, delay)
}

export function $mc(title = '确认删除？', content = '删除后将无法恢复') {
    return new Promise((resolve) => {
        Modal.confirm({
            title,
            icon: createVNode(ExclamationCircleOutlined),
            content,
            onOk: () => resolve()
        });
    })
}
