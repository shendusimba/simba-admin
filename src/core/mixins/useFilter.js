import { ORDER_STATUS } from "~config/enum";

export const $fmtGender = val => ['男', '女'][val]
export const $fmtDate = val => new Date(val).getFormatDate()
export const $fmt2Fixed2Num = (n) => {
    n = parseFloat(n)
    n = n.toFixed(2)
    return parseFloat(n)
}
export const $fmtPrice = (price) => price ? ('¥ ' + price) : '免费'
export const $fmtPayStatus = val => ['未支付', '已支付'][val]
export const $fmtOrderStatus = val => ORDER_STATUS.find(r => r.key === val)?.name