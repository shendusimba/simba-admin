import _ from 'love-u'
export function $go2(path, args) {
    if (args)
        path += ('?' + _.convertObj2Str(args))
    this.$router.push(path)
}