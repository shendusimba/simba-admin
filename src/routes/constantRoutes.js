export default [
    {
        path: '/',
        redirect: '/home',
        component: () => import("@/views/layouts/BaseLayout"),
        meta: {
            title: "数据驾驶舱",
            inMenu: true,
            icon: "i-desktop",
            sortNo: 0
        },
        children: [
            {
                path: "/home",
                meta: {
                    title: "工作平台",
                    icon: "i-desktop",
                    sort: 1,
                    inMenu: true,
                    fixedInBar: true,
                    sortNo: 1
                },
                component: () => import("~views/Home")
            },

        ]
    },
    {
        path: "/launcher",
        meta: {
            title: "启动台",
            icon: "i-desktop",
        },
        component: () => import("~views/launcher")
    },
    {
        path: '/login',
        meta: {
            title: "登录",
            hideInMenu: true,
        },
        component: () => import("~views/login/index")
    },
    {
        path: '/redirect/:path(.*)*',
        meta: {
            title: "跳转中",
            hideInMenu: true,
            hideInTagsView: true
        },
        component: () => import("~views/Redirect")
    },
    {
        path: '/:pathMatch(.*)*',
        meta: {
            hideInMenu: true,
        },
        component: () => import("~views/error/PageNotFound")
    },
]