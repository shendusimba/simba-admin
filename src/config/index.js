import _ from 'love-u'

/**
 * 题目类型
 */
export const SUBJECT_TYPE = {
    DANXUANTI: 0,
    DUOXUANTI: 1,
    PANDUANTI: 2,
    JIANDATI: 3,
};
/**
 * 免登录的白名单
 */
export const WHITE_LIST = ['/login']

/**
 * 角色类型
 */
export const RoleType = {
    GUEST: '0',
    ADMIN: '1'
}

/**
 * 暂无图片的占位图片路径
 */
export const NO_IMG_PATH = "/noimg.jpg"

/**
 * 后台API地址
 */
export const AJAX_BASE_URL = process.env.VUE_APP_AJAX_BASE_URL

/**
 * Socket地址
 */
export const SOCKET_URL = process.env.VUE_APP_SOCKET_URL

/**
 * AJAX超时时间
 */
export const AJAX_TIMEOUT = process.env.VUE_APP_AJAX_TIMEOUT || 5000

/**
 * 静态资源路径
 */
export const STATIC_URL = process.env.VUE_APP_STATIC_URL

/**
 * 文件上传路径
 */
export const UPLOAD_URL = process.env.VUE_APP_UPLOAD_URL

/**
 * 全局网站标题
 */
export const GLOBAL_TITLE = process.env.VUE_APP_GLOBAL_TITLE

/**
 * 版权企业名称
 */
export const COMPANY_NAME = process.env.VUE_APP_COMPANY_NAME

/**
 * 图片上传的限制大小(MB),默认10MB
 */
export const LIMIT_UPLOAD_SIZE = process.env.VUE_APP_LIMIT_UPLOAD_SIZE || 10

/**
 * 登录时存TOKEN的KEY
 */
export const TOKEN_KEY = 'LOGIN-USER-TOKEN'

//加载第三方接口的列表
export const BASE_URL_LIST = _.convertStr2Obj(process.env.VUE_APP_BASE_URL_LIST)