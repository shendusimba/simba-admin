export const GOODS_RECOMMEND_TYPE = [
    {
        key: 0,
        name: "首页",
    },
    {
        key: 1,
        name: "搜索置顶",
    },
    {
        key: 2,
        name: "购物车空白时",
    },
    {
        key: 3,
        name: "订单完成时",
    },
]

/**
 * 订单状态
 */
export const ORDER_STATUS = [
    {
        key: 0,
        name: "待付款",
    },
    {
        key: 1,
        name: "待审核",
    },
    {
        key: 2,
        name: "待发货",
    },
    {
        key: 3,
        name: "待签收",
    },
    {
        key: 4,
        name: "已完成",
    },  
]

// 优惠券模块
 export const  COUPON_MODULE = [
    {
        key: 1,
        name: "满减",
    },
    {
        key: 2,
        name: "直减",
    },
    {
        key: 3,
        name: "折扣",
    },
]

