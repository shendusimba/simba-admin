import { $decorateCols } from '~mixins/useUtils'

export default $decorateCols([
    {
        title: "路径",
        dataIndex: "path",
        align: 'left'
    },
    {
        title: "标题",
        dataIndex: "title",
    },
    {
        title: "组件路径",
        dataIndex: "componentPath",
    },
    {
        title: "是否为菜单",
        dataIndex: "inMenu",
        customRender({ text }) {
            return Number(text) ? '是' : "否"
        }
    },
    {
        title: "图标",
        dataIndex: "icon",
        slots: { customRender: 'icon' }
    },
    {
        title: "排序号",
        dataIndex: "sortNo",
    },
    {
        title: "操作",
        width: 200,
        slots: { customRender: "action" },
    },
])