import { $decorateCols } from '~mixins/useUtils'

export default $decorateCols([
    {
        dataIndex: "id",
        title: "编号",
        width: 180,
    },
    {
        title: "昵称",
        dataIndex: "nickname",
    },
    {
        title: "操作",
        key: "action",
        width: 200,
        slots: { customRender: "action" },
    },
])