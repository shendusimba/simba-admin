


import { $decorateCols } from '~mixins/useUtils'
export default $decorateCols([
    {
        title: "序号",
        customRender: ({ index }) => index + 1
    },
    {
        title: "分类名称",
        dataIndex: "name",
    },
    {
        title: "创建时间",
        dataIndex: "createdAt",
        sbTime: true
    },
    {
        title: "创建人",
        dataIndex: "user.nickname",
    },
    {
        title: "操作",
        width: 220,
        slots: { customRender: "action" },
    },
])