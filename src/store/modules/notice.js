import { getUnReadCount, } from '@/api/notice'
export default {
    namespaced: true,
    state: {
        UNREAD_COUNT: 0,
        NOTICE_LIST: []
    },
    mutations: {
        SET_UNREAD_COUNT(state, val) {
            state.UNREAD_COUNT = val
        },
        SET_NOTICE_LIST(state, val) {
            state.NOTICE_LIST = val
        },
    },
    actions: {
        async getUnReadCount({ commit }) {
            let { success, data } = await getUnReadCount()
            success && commit('SET_UNREAD_COUNT', data)
        },

    }
}