import router from "@/routes";
export default {
  namespaced: true,
  state: {
    collapsed: false,
    isVetical:true,
    opendPages: [],
    activedPage: undefined,
  },
  mutations: {
    TOGGLE_LAYOUT(state) {
      state.isVetical = !state.isVetical;
    },
    TOGGLE_COLLAPSED(state) {
      state.collapsed = !state.collapsed;
    },
    initFixedPages(state, page) {
      let {
        path,
        meta: { title = "无标题", fixedInBar },
      } = page;
      let p = state.opendPages.find((r) => r.path === page.path);
      if (!p) state.opendPages.unshift({ path, title, fixedInBar });
    },
    fixBar(state, path) {
      const p = state.opendPages.find((r) => r.path === path);
      p.fixedInBar = !p.fixedInBar;
    },
    addOpendPages(state, page) {
      let {
        path,
        meta: { title = "无标题", fixedInBar, hideInTagsView },
      } = page;
      if (hideInTagsView) return;
      let p = state.opendPages.find((r) => r.path === path);
      if (!p) state.opendPages.push({ path, title, fixedInBar });
      state.activedPage = path;
    },
    removeOpendPages(state, path) {
      let index = state.opendPages.findIndex((r) => r.path == path);
      state.opendPages.splice(index, 1);
      if (state.activedPage === path) {
        state.activedPage = state.opendPages[index - 1].path;
        router.push(state.activedPage);
      }
    },
    removeOtherPages(state, path) {
      state.opendPages = state.opendPages.filter(
        (item) => item.path === path || item.fixedInBar
      );
    },
    removeAllPages(state) {
      state.opendPages = state.opendPages.filter((r) => r.fixedInBar);
      if (state.opendPages.length) router.push(state.opendPages[0]);
    },
  },
};
