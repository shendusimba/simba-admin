import { $post, $get } from '~mixins/useRequest'
import { getToken, removeToken, setToken } from '~utils/userToken';
import constantRoutes from '~routes/constantRoutes'
import router from '~routes'
import { cloneDeep } from 'lodash'

function sortRoutes(routes) {
    routes.sort((a, b) => a.meta.sortNo - b.meta.sortNo)
    routes.forEach(r => r.children && r.children.length && sortRoutes(r.children))
}

function format2NestedRoutes(routes, pid = 0) {
    return routes.filter(r => r.meta.pid === pid).map(r => {
        r.component = () => import("@/views" + r.componentPath)
        let children = format2NestedRoutes(routes, r.meta.id)
        let res = {
            path: r.path,
            component: r.component,
            meta: r.meta
        }
        if (children) res.children = children
        return res
    })
}

function filter2Menus(routes) {
    return routes.reduce((res, r) => {
        if (r.meta && r.meta.inMenu) {
            if (r.children && r.children.length)
                r.children = filter2Menus(r.children)
            res.push(r)
        }
        return res
    }, [])
}

export default {
    namespaced: true,
    state: {
        token: getToken(),
        userInfo: {},
        menus: [],
        routesExpires: true
    },
    mutations: {
        SET_TOKEN(state, val) {
            state.token = val
        },
        SET_USERINFO(state, val) {
            state.routesExpires = false
            state.userInfo = val
        },
        SET_MENUS(state, val) {
            state.menus = val
        },
        ENABLE_ROUTES_EXPIRES(state) {
            state.routesExpires = true
        },
        LOG_OUT(state) {
            state.userInfo = {}
            state.token = ''
            state.routes = []
            removeToken()
            router.push('/login', { replace: true })
        }
    },
    actions: {
        //根据后台返回的权限数组生成路由和菜单
        async generateRoutes({ commit, state: { userInfo: { routes } } }) {
            //根据Pid改造成嵌套路由
            let allRoutes = format2NestedRoutes(routes).concat(constantRoutes)
            let menus = filter2Menus(cloneDeep(allRoutes))
            sortRoutes(menus)
            commit('SET_MENUS', menus)
            return allRoutes
        },
        getUserInfo({ commit }) {
            return new Promise(async (resolve, reject) => {
                try {
                    let { data } = await $get('/user/getUserInfo')
                    if (data) {
                        commit('SET_USERINFO', data)
                        return resolve()
                    }
                    reject('用户信息不正确,请重新登录！')
                } catch (error) {
                    console.log(error);
                    reject('用户信息不正确,请重新登录！')
                }
            })
        },
        async login({ commit }, loginModel) {
            let { success, data } = await $post('/user/login', loginModel)
            if (success) {
                setToken(data, loginModel.remember ? 1 : null);
                commit('SET_TOKEN', data)
            }
            return success
        },
        logOut({ commit }) {
            commit('LOG_OUT')
            commit('system/removeAllPages', '', { root: true })
        }
    }
}