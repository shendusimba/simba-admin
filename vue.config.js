let { join } = require('path')
const AntdDayjsWebpackPlugin = require('antd-dayjs-webpack-plugin')

let resolve = (...path) => join(__dirname, 'src', ...path)

module.exports = {
	lintOnSave: 'warning',
	outputDir: process.env.VUE_APP_OUTPUT || 'dist',
	productionSourceMap: false,
	publicPath: '/',
	devServer: {
		host: "127.0.0.1",
		port: 888,
		open: true,
	},
	configureWebpack: {
		externals: {
			vue: "Vue",
			vuex: "Vuex",
			'vue-router': 'VueRouter'
		},
		plugins: [
			new AntdDayjsWebpackPlugin({
				preset: 'antdv3'
			})
		],
		//关闭 webpack 的性能提示，如果打包后文件过大，打包会失败
		performance: {
			hints: 'warning',
			//入口起点的最大体积
			maxEntrypointSize: 50000000,
			//生成文件的最大体积
			maxAssetSize: 30000000,
			//只给出 js 文件的性能提示
			assetFilter: function (assetFilename) {
				return assetFilename.endsWith('.js');
			}
		}
	},
	chainWebpack: config => {
		config.resolve.alias
			.set('~views', resolve('views'))
			.set('~com', resolve('components'))
			.set('~assets', resolve('assets'))
			.set('~utils', resolve('utils'))
			.set('~rules', resolve('rules'))
			.set('~routes', resolve('routes'))
			.set('~store', resolve('store'))
			.set('~core', resolve('core'))
			.set('~vendor', resolve('vendor'))
			.set('~config', resolve('config'))
			.set('~mixins', resolve('core', 'mixins'))
			.set('~simbajs', resolve('core', 'simba'))
			.set('~api', resolve('api'))
			.set('vue-i18n', 'vue-i18n/dist/vue-i18n.runtime.esm-bundler.js')

		config
			.plugin('html')
			.tap(args => {
				args[0].title = process.env.VUE_APP_TITLE
				return args
			})

	},

}
