# Simba-Admin-Vue
> 基于```Vue3+AntDV2```的后台管理系统基础开发框架，提供了大量封装，提高开发效率

[体验地址](http://admin.simbajs.com/)
[上手教程](https://www.bilibili.com/video/BV16q4y1d7NM)
[ShenduStudio](http://simbajs.com/)

```
成为Simba-SVIP,可获取从0搭建框架视频教程、全力技术支持以及定制组件

Q群:534937912   vx:simba_js

祝开卷有益。
```


